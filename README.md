# Read me

## Table of contents

- [Read me](#read-me)
  - [Table of contents](#table-of-contents)
  - [General info](#general-info)
  - [Technologies](#technologies)
  - [Setup](#setup)
  - [More](#more)

## General info

This project is used for converting UX design examples into an HTML prototype

## Technologies

Project is created with:

- HTML5
- CSS3
- JavaScript (Twitter Bootstrap 5, Javascript ES6)
- NodeJS (optional local HTTP server)

## Setup

To run this project, install it locally using npm:

```bash
cd prototype
npm install http-server
npm start
```

## More

The HTTP server can be started with more options. Check `http-server -h`. If needed, replace `npm start` with a custom command like `http-server -a localhost -p 8000 -c-1`
