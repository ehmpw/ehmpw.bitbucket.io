function outputUpdate(amount) {

	let outControl = document.getElementById('high-cost-gross'),
		inControl = document.getElementById('high-cost-control'),
		calculatedControl = document.getElementById('high-cost-calculated'),
		bargainControl = document.getElementById('high-cost-bargain'),
		customerMessage = document.getElementById('range-control-step'),
		costumerPayPercent = 100

  outControl.value = amount;
	inControl.value = amount;

	// 100% | 0% discount
	const pay100 = 1300
	// 50% | 50% discount
  const pay50 = 2481
  const pay100to50 = (pay50 - pay100)*0.5
	// 25% | 75% discount
	const pay25 = 4610
  const pay50to25 = (pay25 - pay50)*0.25
	// 10% | 90% discount
	const pay10 = 6381
  const pay25to10 = (pay10 - pay25)*0.1

	// if (inControl.value > pay100 && inControl.value < pay50) {
	// 	console.log('✅ inControl.value is between pay100 and pay50');
	// } else {
	// 	console.log('⛔️ inControl.value is NOT between pay100 and pay50');
	// }

	if (amount < pay100) {
		costumerPayPercent = 100;

		// you pay (sek)
    calculatedControl.innerHTML = amount;
		// discount (sek)
		bargainControl.innerHTML = 0;

		document.getElementById('range-control-cost').style.borderColor = '#feeaed';
		document.getElementById('range-control-step').style.backgroundColor = '#940e5c';
		customerMessage.textContent = 'Du betalar 100% av kostnaden';
	}
	if (amount > pay100 && amount < pay50) {
		costumerPayPercent = 50;

		// you pay (sek)
    calculatedControl.innerHTML = (pay100 + ((costumerPayPercent * 0.01) * (amount - pay100))).toFixed();
		// discount (sek)
    bargainControl.innerHTML = (amount - calculatedControl.innerHTML);

		document.getElementById('range-control-cost').style.borderColor = '#feecc4';
		document.getElementById('range-control-step').style.backgroundColor = '#eab34e';
    customerMessage.innerHTML = 'Du betalar 50% av kostnaden <br> mellan ' + pay100 + ' kronor och ' + pay50 + ' kronor';
	}
	if (amount > pay50 && amount < pay25) {
		costumerPayPercent = 25;

		// you pay (sek)
    calculatedControl.innerHTML = (pay100 + pay100to50 + ((costumerPayPercent * 0.01) * (amount - pay100 + pay100to50))).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (amount - calculatedControl.innerHTML).toFixed();

		document.getElementById('range-control-cost').style.borderColor = '#e6f8ea';
		document.getElementById('range-control-step').style.backgroundColor = '#00a03d';
    customerMessage.innerHTML = 'Du betalar 25% av kostnaden <br> mellan ' + pay50 + ' kronor och ' + pay25 + ' kronor';
	}
	if (amount > pay25 && amount < pay10) {
		costumerPayPercent = 10;

		// you pay (sek)
    calculatedControl.innerHTML = (pay100 + pay100to50 + pay50to25 + ((costumerPayPercent * 0.01) * (amount - pay100 + pay100to50 + pay50to25))).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (amount - calculatedControl.innerHTML).toFixed();

		document.getElementById('range-control-cost').style.borderColor = '#e2f1fc';
		document.getElementById('range-control-step').style.backgroundColor = '#1d6dca';
    customerMessage.innerHTML = 'Du betalar 10% av kostnaden <br> mellan ' + pay25 + ' kronor och ' + pay10 + ' kronor';
	}
	if (amount > pay10) {
		costumerPayPercent = 0;

		// you pay (sek)
    calculatedControl.innerHTML = (pay100 + pay100to50 + pay50to25 + pay25to10 + ((costumerPayPercent * 0.01) * (amount - pay100 + pay100to50 + pay50to25 + pay25to10))).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (amount - calculatedControl.innerHTML).toFixed();

		document.getElementById('range-control-cost').style.borderColor = 'transparent'
		document.getElementById('range-control-step').style.backgroundColor = '#d0192b'
		customerMessage.textContent = 'Du har rätt till frikort'
	}
	// console.log('you pay ' + costumerPayPercent + '%');

	// // you pay (sek)
	// calculatedControl.innerHTML = (amount * ((100 - costumerPayPercent) * 0.01)).toFixed();

	// 	// discount (sek)
	// 	bargainControl.innerHTML = (amount * (costumerPayPercent * 0.01)).toFixed();

}
