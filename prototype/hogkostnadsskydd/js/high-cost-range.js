function outputUpdate(amount) {
	let outControl = document.getElementById("high-cost-gross"),
		inControl = document.getElementById("high-cost-control"),
		rangeControl = document.getElementById("range-control-cost"),
		calculatedControl = document.getElementById("high-cost-calculated"),
		bargainControl = document.getElementById("high-cost-bargain"),
		customerMessage = document.getElementById("range-control-step"),
		costumerPayPercent = 100;

	outControl.value = amount;
	inControl.value = amount;

	// pay 100% | 0% discount
	const discount0 = 1300;
	// pay 50% | 50% discount
	const discount50 = 2481;
	const discount0to50 = (discount50 - discount0) * 0.5;
	// pay 25% | 75% discount
	const discount75 = 4610;
	const discount50to75 = (discount75 - discount50) * 0.25;
	// pay 10% | 90% discount
	const discount90 = 6381;
	const discount75to90 = (discount90 - discount75) * 0.1;

	// if (inControl.value > discount0 && inControl.value < discount50) {
	// 	console.log('✅ inControl.value is between discount0 and discount50');
	// } else {
	// 	console.log('⛔️ inControl.value is NOT between discount0 and discount50');
	// }

	// console.log(amount);

	if (amount <= discount0) {
		costumerPayPercent = 100 * 0.01;

		// you pay (sek)
		calculatedControl.innerHTML = amount || 0;
		// discount (sek)
		bargainControl.innerHTML = 0;

		rangeControl.style.borderColor = "#feeaed";
		customerMessage.style.backgroundColor = "#fff";
		customerMessage.style.color = "#940e5c";
		customerMessage.style.border = "2px solid #940e5c";
		customerMessage.innerHTML =
			"Du får betala fullt pris <br> mellan 0 (noll) kronor och " +
			discount0 +
			" kronor";
	}
	if (amount > discount0 && amount <= discount50) {
		costumerPayPercent = 50 * 0.01;

		// you pay (sek)
		calculatedControl.innerHTML = (
			discount0 +
			costumerPayPercent * (amount - discount0)
		).toFixed();
		// discount (sek)
		bargainControl.innerHTML = amount - calculatedControl.innerHTML;

		rangeControl.style.borderColor = "#feeff2";
		customerMessage.style.backgroundColor = "#f7e3ed";
		customerMessage.style.color = "#940e5c";
		customerMessage.style.border = "2px solid #940e5c";
		customerMessage.innerHTML =
			"Du får 50% rabatt <br> mellan " +
			discount0 +
			" kronor och " +
			discount50 +
			" kronor";
	}
	if (amount > discount50 && amount <= discount75) {
		costumerPayPercent = 25 * 0.01;

		// you pay (sek)
		calculatedControl.innerHTML = (
			discount0 +
			discount0to50 +
			costumerPayPercent * (amount - discount50)
		).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (
			amount - calculatedControl.innerHTML
		).toFixed();

		rangeControl.style.borderColor = "#fff5f6";
		customerMessage.style.backgroundColor = "#a47f9b";
		customerMessage.style.color = "#fff";
		customerMessage.style.border = "2px solid #940e5c";
		customerMessage.innerHTML =
			"Du får 75% rabatt <br> mellan " +
			discount50 +
			" kronor och " +
			discount75 +
			" kronor";
	}
	if (amount > discount75 && amount <= discount90) {
		costumerPayPercent = 10 * 0.01;

		// you pay (sek)
		calculatedControl.innerHTML = (
			discount0 +
			discount0to50 +
			discount50to75 +
			costumerPayPercent * (amount - discount75)
		).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (
			amount - calculatedControl.innerHTML
		).toFixed();

		rangeControl.style.borderColor = "#fffafb";
		customerMessage.style.backgroundColor = "#854674";
		customerMessage.style.color = "#fff";
		customerMessage.style.border = "2px solid #940e5c";
		customerMessage.innerHTML =
			"Du får 90% rabatt <br> mellan " +
			discount75 +
			" kronor och " +
			discount90 +
			" kronor";
	}
	if (amount > discount90) {
		costumerPayPercent = 0;

		// you pay (sek)
		calculatedControl.innerHTML = (
			discount0 +
			discount0to50 +
			discount50to75 +
			discount75to90
		).toFixed();
		// discount (sek)
		bargainControl.innerHTML = (
			amount - calculatedControl.innerHTML
		).toFixed();

		rangeControl.style.borderColor = "transparent";
		customerMessage.style.backgroundColor = "#6e0e50";
		customerMessage.style.color = "#fff";
		customerMessage.style.border = "2px solid #940e5c";
		customerMessage.innerHTML =
			"Du har rätt till frikort <br> på köp över " +
			discount90 +
			" kronor";
	}
	// console.log('you pay ' + 100 * costumerPayPercent + '%');

	// // you pay (sek)
	// calculatedControl.innerHTML = (amount * ((100 - costumerPayPercent) * 0.01)).toFixed();

	// 	// discount (sek)
	// 	bargainControl.innerHTML = (amount * (costumerPayPercent * 0.01)).toFixed();

	// console.log("inControl.value: " + inControl.value);

	// console.log(
	//   "costumerPayPercent: " + costumerPayPercent + "\n",
	// 	"discount0: " + discount0 + "\n",
	//   "discount0to50: " + discount0to50 + "\n",
	//   "discount50to75: " + discount50to75 + "\n",
	//   "discount0 + discount0to50 + discount50to75: " + (discount0 + discount0to50 + discount50to75) + "\n"
	// )
}
