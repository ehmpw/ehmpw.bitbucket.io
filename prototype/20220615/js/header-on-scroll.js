// Source: https://codepen.io/ducblog/pen/oqQaqQ

// var isIOS = /iPad|iPhone|iPod/.test(navigator.platform) ||
// 	(navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)

var largerThanMobileSize
if (window.innerWidth > 576) {
	largerThanMobileSize = true
} else {
	largerThanMobileSize = false
}

var lastKnownScrollY = 0
var currentScrollY = 0
var ticking = false
const idOfHeader = 'default-header'
var eleHeader = null
var classes = {
	pinned: 'header-pin',
	unpinned: 'header-unpin',
	fixed: 'position-fixed',
	absolute: 'position-absolute',
}

function onScroll() {
	currentScrollY = window.pageYOffset
	requestTick()
}

function requestTick() {
	if (!ticking) {
		requestAnimationFrame(update)
	}
	ticking = true
}

function update() {
	if (largerThanMobileSize && currentScrollY < lastKnownScrollY) {
		pin()
	} else if (largerThanMobileSize && currentScrollY > lastKnownScrollY) {
		unpin()
	}
	lastKnownScrollY = currentScrollY
	ticking = false
}

function pin() {
	if (eleHeader.classList.contains(classes.unpinned)) {
		eleHeader.classList.remove(classes.unpinned)
		eleHeader.classList.add(classes.pinned)
	}
}

function unpin() {
	if (
		eleHeader.classList.contains(classes.pinned) ||
		!eleHeader.classList.contains(classes.unpinned)
	) {
		eleHeader.classList.remove(classes.pinned)
		eleHeader.classList.add(classes.unpinned)
	}
}

window.onload = function () {
	eleHeader = document.getElementById(idOfHeader)
	document.addEventListener('scroll', onScroll, false)

	if (largerThanMobileSize == false) {
		if (
			eleHeader.classList.contains(classes.fixed) ||
			!eleHeader.classList.contains(classes.absolute)
		) {
			// console.log('largerThanMobileSize = ' + largerThanMobileSize);
			eleHeader.classList.remove(classes.fixed)
			eleHeader.classList.add(classes.absolute)
		}
	} else {
		if (
			eleHeader.classList.contains(classes.absolute) ||
			!eleHeader.classList.contains(classes.fixed)
		) {
			// console.log('largerThanMobileSize = ' + largerThanMobileSize);
			eleHeader.classList.add(classes.fixed)
			eleHeader.classList.remove(classes.absolute)
		}
	}
}

window.onscroll = function () {
	if (largerThanMobileSize && window.pageYOffset < 1) {
		//console.log('back to top');
		pin()
	}
	//console.log(window.pageYOffset);
}
