// https://codepen.io/desandro/pen/ooEZyj
// external js: isotope.pkgd.js

var grid = document.querySelector('.grid');
var filterButtonGroup = document.querySelector('.filter-button-group');
var iso;

function onHashchange() {
	var hashFilter = getHashFilter();
	if (!hashFilter && iso) {
		return;
	}
	if (!iso) {
		// init Isotope for first time
		iso = new Isotope(grid, {
			itemSelector: '.element-item',
			layoutMode: 'masonry',
			filter: hashFilter || '',
		});
	} else {
		// just filter with hash
		iso.arrange({
			filter: hashFilter
		});
	}

	// set selected class on button
	if (hashFilter) {
		var checkedButton = filterButtonGroup.querySelector('.active');
		if (checkedButton) {
			checkedButton.classList.remove('active');
		}
		filterButtonGroup.querySelector('[data-filter="' + hashFilter + '"]').classList.add('active');
	}
}

window.addEventListener('hashchange', onHashchange);
// trigger event handler to init Isotope
onHashchange();


// bind filter button click
filterButtonGroup.addEventListener('click', function(event) {
	var filterAttr = event.target.getAttribute('data-filter');
	if (!filterAttr) {
		return;
	}
	location.hash = 'filter=' + encodeURIComponent(filterAttr);

	// remove first
	document.querySelectorAll('.checkbox-icon').forEach(function(a) {
		a.remove()
	})

	// then add
	var checked = document.createElement("span");
	checked.setAttribute("class", "material-icons ps-2 float-end checkbox-icon");
	checked.innerHTML = "done";

	event.target.appendChild(checked);

});

function getHashFilter() {
	// get filter=filterName
	var matches = location.hash.match(/filter=([^&]+)/i);
	var hashFilter = matches && matches[1];
	return hashFilter && decodeURIComponent(hashFilter);
}
