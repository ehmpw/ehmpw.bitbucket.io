// Bootstrap tooltips - START
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
	return new bootstrap.Tooltip(tooltipTriggerEl)
})
// Bootstrap tooltips - STOP

// Remove element on event (like click)
function remove(el) {
	var element = el;
	element.remove();
}

// // Make page dim nicely when search is in focus

// function mainSearchFocus() {
// 	// console.log("main search is in focus")
// 	var elMain = document.getElementById("default-main");
// 	elMain.classList.add("search-in-focus");
// 	// elMain.style.zIndex = "-2"
// 	// elMain.style.position = "relative";
// 	var elHeader = document.getElementById("default-header");
// 	elHeader.classList.add("search-in-focus");
// 	// elHeader.style.zIndex = "-1";
// 	// elHeader.style.position = "relative";
// 	var elNav = document.getElementById("main-nav");
// 	elNav.classList.add("search-in-focus");
// }

// function mainSearchBlur() {
// 	// console.log("main search is not in focus")
// 	var elMain = document.getElementById("default-main");
// 	elMain.classList.remove("search-in-focus");
// 	// elMain.style.zIndex = "0";
// 	// elMain.style.position = "initial";
// 	var elHeader = document.getElementById("default-header");
// 	elHeader.classList.remove("search-in-focus");
// 	// elHeader.style.zIndex = "0";
// 	// elHeader.style.position = "initial";
// 	var elNav = document.getElementById("main-nav");
// 	elNav.classList.remove("search-in-focus");
// }

// var hamburgerToggler = document.querySelector(".hamburger");
// // Detect all clicks on the document
// document.addEventListener("click", function(event) {
// 	// If user clicks inside the element, do nothing
// 	if (event.target.closest(".hamburger-toggler")) return;
// 	// If user clicks outside the element, hide it!
// 	hamburgerToggler.classList.remove("active");
// });
