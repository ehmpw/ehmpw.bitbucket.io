// Source: https://codepen.io/ducblog/pen/oqQaqQ

var isIOS = /iPad|iPhone|iPod/.test(navigator.platform) ||
	(navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)

var lastKnownScrollY = 0;
var currentScrollY = 0;
var ticking = false;
var idOfHeader = 'default-header';
var eleHeader = null;
const classes = {
	pinned: 'header-pin',
	unpinned: 'header-unpin',
};

function onScroll() {
	currentScrollY = window.pageYOffset;
	requestTick();
}

function requestTick() {
	if (!ticking) {
		requestAnimationFrame(update);
	}
	ticking = true;
}

function update() {
	if (!isIOS && currentScrollY < lastKnownScrollY) {
		pin();
	} else if (!isIOS && currentScrollY > lastKnownScrollY) {
		unpin();
	}
	lastKnownScrollY = currentScrollY;
	ticking = false;
}

function pin() {
	if (eleHeader.classList.contains(classes.unpinned)) {
		eleHeader.classList.remove(classes.unpinned);
		eleHeader.classList.add(classes.pinned);
	}
}

function unpin() {
	if (eleHeader.classList.contains(classes.pinned) || !eleHeader.classList.contains(classes.unpinned)) {
		eleHeader.classList.remove(classes.pinned);
		eleHeader.classList.add(classes.unpinned);
	}
}

window.onload = function() {
	eleHeader = document.getElementById(idOfHeader);
	document.addEventListener('scroll', onScroll, false);
}

window.onscroll = function() {
	if (window.pageYOffset < 1) {
		// console.log('back to top');
		eleHeader.classList.remove(classes.pinned);
	}
};
