// Bootstrap tooltips - START

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
	return new bootstrap.Tooltip(tooltipTriggerEl)
})
// Remove element on event (like click)
function remove(el) {
	var element = el;
	element.remove();
}

// Bootstrap tooltips - STOP
