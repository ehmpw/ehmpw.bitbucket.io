// Load this script via src="..." with `async` and `defer` attributes
// so that it'll run before the user gets to interact with the page,
// after the DOM has been constructed.

const first = document.getElementsByName("first-value")[0];
const second = document.getElementsByName("second-value")[0];
const initial = second.innerHTML;

// Either hard code this, or get it on page load, just make sure
// it's already available before users start picking values!
const optionMap = {
	Vaccinationsbevis: [
		"Bevis till barn",
		"Blanketter",
		"Felaktigt antal doser",
		"Giltighetstid",
		"Reservnummer",
		"Samordningsnummer",
		"Skaffa ett vaccinationsbevis",
		"Uppdatera ditt bevis",
		"Utlandsvaccinerad",
		"Övriga frågor",
	],
	Testbevis: [
		"Bevis till barn",
		"Giltighetstid",
		"Skaffa ett testbevis",
		"Övriga frågor",
	],
	Tillfrisknandebevis: [
		"Bevis till barn",
		"Giltighetstid",
		"Skaffa ett tillfrisknandebevis",
		"Övriga frågor",
	],
	Övrigt: ["Felanmälan", "Förbättringsförslag", "Övriga frågor"],
};

function addOption(selectElement, text) {
	const option = document.createElement("option");
	option.value = text;
	option.textContent = text;
	selectElement.append(option);
}

// Fill the first selector
Object.keys(optionMap).forEach((text) => addOption(first, text));

// And only fill the second selector when we know the first value
first.addEventListener("change", (evt) => {
	const secondWrapper = document.getElementById("second-wrapper");
	if (secondWrapper.style.display === "none") {
		secondWrapper.style.display = "block";
	}

	second.innerHTML = initial;
	optionMap[evt.target.value].forEach((text) => addOption(second, text));
});

/* Detect URL parameter - START */

// Get URL string
const params = new Proxy(new URLSearchParams(window.location.search), {
	get: (searchParams, prop) => searchParams.get(prop),
});
// Get the value of "some_key" in eg "https://example.com/?some_key=some_value"
let s_value = params.ref; // "s_value"
// console.log(s_value);

if (
	s_value === "vaccinationsbevis" ||
	s_value === "tillfrisknandebevis" ||
	s_value === "testbevis"
) {
	const secondWrapper = document.getElementById("second-wrapper");
	if (secondWrapper.style.display === "none") {
		secondWrapper.style.display = "block";
	}
	const selector = document.querySelector("#second-value");
	selector.classList.add("highlighted");
}

if (s_value === "vaccinationsbevis") {
	// Get the select menu
	const firstValue = document.querySelector("#first-value");
	// Sets the selected value to matching select option
	firstValue.value = "Vaccinationsbevis";

	second.innerHTML = initial;
	optionMap[firstValue.value].forEach((text) => addOption(second, text));
}

if (s_value === "tillfrisknandebevis") {
	// Get the select menu
	const firstValue = document.querySelector("#first-value");
	// Sets the selected value to matching select option
	firstValue.value = "Tillfrisknandebevis";

	second.innerHTML = initial;
	optionMap[firstValue.value].forEach((text) => addOption(second, text));
}

if (s_value === "testbevis") {
	// Get the select menu
	const firstValue = document.querySelector("#first-value");
	// Sets the selected value to matching select option
	firstValue.value = "Testbevis";

	second.innerHTML = initial;
	optionMap[firstValue.value].forEach((text) => addOption(second, text));
}

/* Detect URL parameter - STOP */

// Show submit button
second.addEventListener("change", (evt) => {
	const thirdWrapper = document.getElementById("third-wrapper");
	if (thirdWrapper.style.display === "none") {
		thirdWrapper.style.display = "block";
	}
});

// Show form
let myButton = document.getElementById("button-01");
myButton.addEventListener("click", function (e) {
	const fourthWrapper = document.getElementById("fourth-wrapper");
	if (fourthWrapper.style.display === "none") {
		fourthWrapper.style.display = "block";
	}
	// Hide submit button
	const thirdWrapper = document.getElementById("third-wrapper");
	if (thirdWrapper.style.display === "block") {
		thirdWrapper.style.display = "none";
	}

	const selector = document.querySelector("#second-value");
	selector.classList.remove("highlighted");
});
