// Bootstrap tooltips - START
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
	return new bootstrap.Tooltip(tooltipTriggerEl)
})
// Bootstrap tooltips - STOP

// Remove element on event (like click)
function remove(el) {
	var element = el;
	element.remove();
}

// Respect dark mode - START
const prefersDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;

if (prefersDarkMode) {
	document.body.classList.add('dark-mode');
} else {
	document.body.classList.add('light-mode');
}
// Respect dark mode - STOP