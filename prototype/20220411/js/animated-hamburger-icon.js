// (c) 2020-2021 Written by Simon Köhler in Panama
// github.com/koehlersimon
// simon-koehler.com

document.addEventListener('click', function(e) {

	// Click hamburger to activate
	if (e.target.classList.contains('hamburger-toggle')) {
		e.target.children[0].classList.toggle('active');
	} // else
	// console.dir(this);
	// note e.target can be a nested element, not the body element, resulting in misfires
	// console.log(e.target);

	// Click anywhere to close hamburger (if active)
	if (e.target.querySelector("a:not(#nav-link-main-menu)")) {
		const hamburgerToggle = document.querySelector("#nav-link-main-menu>.hamburger-toggle>.hamburger");
		hamburgerToggle.classList.remove("active");
	}

	if (e.target.classList.contains('mobile-hamburger-link')) {
		e.target.children[0].children[0].classList.toggle('active');
	}

})
