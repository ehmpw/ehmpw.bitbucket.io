// document.addEventListener('DOMContentLoaded', function() {
// 	console.log('DOM is fully loaded and parsed');
// });

// deep linking - load tab on refresh
// let url = location.href.replace(/\/$/, '');
var url = location.href;
if (location.hash) {
	var hash = url.split('#');
	var currentTab = document.querySelector('#main-tab a[href="#' + hash[1] + '"]');
	var curTab = new bootstrap.Tab(currentTab);
	curTab.show();
	// url = location.href.replace(/\/#/, '#');
	history.replaceState(null, null, url);
	setTimeout(function() {
		window.scrollTop = 0;
	}, 400);
}
// change url based on selected tab
var selectableTabList = [].slice.call(document.querySelectorAll('a[data-bs-toggle="tab"]'));
selectableTabList.forEach(function(selectableTab) {
	// var selTab = new bootstrap.Tab(selectableTab);
	selectableTab.addEventListener('click', function() {
		var newUrl;
		var hash = selectableTab.getAttribute('href');
		if (hash == '#privat-tab') {
			newUrl = url.split('#')[0];
		} else {
			newUrl = url.split('#')[0] + hash;
		}
		history.replaceState(null, null, newUrl);
	});
});
