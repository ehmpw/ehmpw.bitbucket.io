const nav = document.querySelector('#default-header')
const navTop = nav.offsetTop

function stickyNavigation() {
	// console.log('navTop = ' + navTop)
	// console.log('scrollY = ' + window.scrollY)

	if (window.scrollY > navTop) {
		// nav offsetHeight = height of nav
		document.body.style.paddingTop = nav.offsetHeight + 'px'
		document.body.classList.add('fixed-nav')
	} else {
		document.body.style.paddingTop = 0
		document.body.classList.remove('fixed-nav')
	}
}

window.addEventListener('scroll', stickyNavigation)

// Disable sticky header on menu-click - START

var navLinkMainMenu = document.querySelector('#nav-link-main-menu')

navLinkMainMenu.onclick = function () {
	document.body.style.paddingTop = 0
  document.body.classList.toggle('mega-menu-opened')
  window.scrollTo({top: 0, behavior: 'smooth'});
}

// Disable sticky header on menu-click - STOP
