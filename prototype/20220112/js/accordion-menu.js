var accordionMenu = function() {

	/**
	 * Element.closest() polyfill
	 * https://developer.mozilla.org/en-US/docs/Web/API/Element/closest#Polyfill
	 */
	if (!Element.prototype.closest) {
		if (!Element.prototype.matches) {
			Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
		}
		Element.prototype.closest = function(s) {
			var el = this;
			var ancestor = this;
			if (!document.documentElement.contains(el)) return null;
			do {
				if (ancestor.matches(s)) return ancestor;
				ancestor = ancestor.parentElement;
			} while (ancestor !== null);
			return null;
		};
	}

	// Listen for click on the document
	// Accordion menu functionality
	document.addEventListener('click', function(event) {

		// Bail if our clicked element doesn't match
		var trigger = event.target.closest('[data-accordion-menu]');
		if (!trigger) return;

		// Get the target content
		var target = document.querySelector(trigger.hash);
		if (!target) return;

		// Prevent default link behavior
		event.preventDefault();

		// Toggle our content
		target.classList.toggle('accordion-menu--hidden');

		// Toggle trigger class
		trigger.classList.toggle('accordion-menu--active');
	});

};

accordionMenu();

// Open menu items
document.addEventListener("DOMContentLoaded", function() {
	var menu_opener_link = document.querySelectorAll("[href='#menu-1'],[href='#menu-4'],[href='#menu-6']");
	[].forEach.call(menu_opener_link, function(el) {
		el.classList.add("accordion-menu--active");
	});
	var menu_opener_dropdown = document.querySelectorAll(".accordion-menu--hidden#menu-1,.accordion-menu--hidden#menu-4,.accordion-menu--hidden#menu-6");
	[].forEach.call(menu_opener_dropdown, function(el) {
		el.classList.remove("accordion-menu--hidden");
	});

});
