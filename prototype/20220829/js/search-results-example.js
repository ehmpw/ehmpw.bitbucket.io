// https: //codepen.io/KennyGHanson/pen/xmGeqO

// configuration variables
var itemsPerPage = 5;

// reference to keep track of current page
var currentPage = 1;

// data, could be json from api
var results = [{
	title: "Knight of the Widget",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tecum optime, deinde etiam cum mediocri amico. Facillimum id quidem est, inquam. Tenent mordicus. Eaedem res maneant alio modo. Eadem nunc mea adversum te oratio est. Inquit, dasne adolescenti veniam?",
	type: "web"
}, {
	title: "Oddly Uneven",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Bestiarum vero nullum iudicium puto. Beatum, inquit. Quis hoc dicit? At enim hic etiam dolore. Non risu potius quam oratione eiciendum? Duo Reges: constructio interrete.",
	type: "description"
}, {
	title: "Midlife Upgrade",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nemo igitur esse beatus potest. Illa videamus, quae a te de amicitia dicta sunt. Duo Reges: constructio interrete. Nulla erit controversia.",
	type: "newspaper"
}, {
	title: "Old Guard",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam, ut sint illa vendibiliora, haec uberiora certe sunt. Illud non continuo, ut aeque incontentae. At iste non dolendi status non vocatur voluptas. Poterat autem inpune; Duo Reges: constructio interrete.",
	type: "web"
}, {
	title: "Ordinary Pony",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Deprehensus omnem poenam contemnet. Idem iste, inquam, de voluptate quid sentit? Sed tamen intellego quid velit.",
	type: "description"
}, {
	title: "Rhino",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nimis multa. Poterat autem inpune; Non enim iam stirpis bonum quaeret, sed animalis. Quod cum dixissent, ille contra. Explanetur igitur. Duo Reges: constructio interrete.",
	type: "newspaper"
}, {
	title: "Riveting Rigger",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quod ea non occurrentia fingunt, vincunt Aristonem; Quo igitur, inquit, modo? An eiusdem modi? Quae cum dixisset, finem ille. Tecum optime, deinde etiam cum mediocri amico. Duo Reges: constructio interrete. Traditur, inquit, ab Epicuro ratio neglegendi doloris.",
	type: "web"
}, {
	title: "Rules Lawyer",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec in pueris; Quo modo autem philosophus loquitur? Primum quid tu dicis breve? Nosti, credo, illud: Nemo pius est, qui pietatem-; Duo Reges: constructio interrete. Quorum altera prosunt, nocent altera.",
	type: "description"
}, {
	title: "Sacrifice Play",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quae diligentissime contra Aristonem dicuntur a Chryippo. Duo Reges: constructio interrete. Nosti, credo, illud: Nemo pius est, qui pietatem-; At multis se probavit.",
	type: "newspaper"
}, {
	title: "Knight of the Widget",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et nemo nimium beatus est; Hoc loco tenere se Triarius non potuit. Quis istud possit, inquit, negare? Cur igitur, cum de re conveniat, non malumus usitate loqui? Nam ante Aristippus, et ille melius.",
	type: "web"
}, {
	title: "Oddly Uneven",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quod vestri non item. Non autem hoc: igitur ne illud quidem. Duo Reges: constructio interrete. Negat enim summo bono afferre incrementum diem. Sed tamen intellego quid velit.",
	type: "description"
}, {
	title: "Midlife Upgrade",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid ergo attinet gloriose loqui, nisi constanter loquare? Sit sane ista voluptas. Quae est igitur causa istarum angustiarum? Nunc agendum est subtilius.",
	type: "newspaper"
}, {
	title: "Old Guard",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. At iam decimum annum in spelunca iacet. Certe non potest. Tamen a proposito, inquam, aberramus. Sed nunc, quod agimus; Duo Reges: constructio interrete. Vestri haec verecundius, illi fortasse constantius.",
	type: "web"
}, {
	title: "Ordinary Pony",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Idemne potest esse dies saepius, qui semel fuit? Sed nimis multa.",
	type: "description"
}, {
	title: "Rhino",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Immo alio genere; Nunc haec primum fortasse audientis servire debemus. Quid sequatur, quid repugnet, vident. Age sane, inquam. Laboro autem non sine causa;",
	type: "newspaper"
}, {
	title: "Riveting Rigger",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Illa argumenta propria videamus, cur omnia sint paria peccata. Quid, quod res alia tota est? Graccho, eius fere, aequalí? Tibi hoc incredibile, quod beatissimum. Pollicetur certe. Duo Reges: constructio interrete. Quid autem habent admirationis, cum prope accesseris? Beatus sibi videtur esse moriens. Quodsi ipsam honestatem undique pertectam atque absolutam.",
	type: "web"
}, {
	title: "Random Words",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid sequatur, quid repugnet, vident. At multis malis affectus. Duo Reges: constructio interrete. An tu me de L. Simus igitur contenti his.",
	type: "description"
}, {
	title: "Sacrifice Play",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nihil opus est exemplis hoc facere longius. Quid dubitas igitur mutare principia naturae? Eadem fortitudinis ratio reperietur. Ubi ut eam caperet aut quando? Duo Reges: constructio interrete.",
	type: "newspaper"
}, {
	title: "Rules Lawyer",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Itaque contra est, ac dicitis; Tum Triarius: Posthac quidem, inquit, audacius. Vide, quantum, inquam, fallare, Torquate. Bonum liberi: misera orbitas. Quae ista amicitia est? Duo Reges: constructio interrete. Non potes, nisi retexueris illa.",
	type: "description"
}, {
	title: "Sacrifice Play",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Minime vero istorum quidem, inquit. Maximus dolor, inquit, brevis est. Ac tamen hic mallet non dolere. Sint modo partes vitae beatae.",
	type: "newspaper"
}, {
	title: "Shaggy Camel",
	body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Equidem, sed audistine modo de Carneade? Sint ista Graecorum; Qui-vere falsone, quaerere mittimus-dicitur oculis se privasse; Proclivi currit oratio.",
	type: "description"
}]

// reference to total pages
var pages = numPages(results)

function numPages(resultsArray) {
	// returns the number of pages
	return Math.ceil(resultsArray.length / itemsPerPage)
}

function changePage(page) {
	// reference to output containing element
	var output = document.getElementById("output")

	// make sure page is in bounds
	if (page < 1) page = 1
	if (page > pages) page = pages

	// clear output containing element
	output.innerHTML = ""

	// for each item within the range of the current page
	for (var i = (page - 1) * itemsPerPage; i < (page * itemsPerPage) && i < results.length; i++) {
		// append the html to the output containing element
		// output.innerHTML += `<li>${results[i].title} (${results[i].body})</li>`
		output.innerHTML += `
			<a href="" title="this link lacks proper description">
				<div class="card rounded-3 shadow-sm-heavy mb-4 element-item w-100 ${results[i].type}">
					<div class="card-body pb-0">
						<div class="card-text w-100">
							<dl class="row my-0">
								<dt class="col card-title h6 fw-bold link-color">${results[i].title}</dt>
								<dd class="col-1 card-title text-nowrap text-end text-muted">#${i+1}</dd>
								<dd class="col-12 pb-2 card-text">${results[i].body}</dd>
								<dd class="col-sm-8">
									<span class="material-icons float-start pe-2">${results[i].type}</span>
									<ol class="breadcrumb">
										<li class="breadcrumb-item">Home</li>
										<li class="breadcrumb-item">Library</li>
										<li class="breadcrumb-item active">Data</li>
									</ol>
								</dd>
								<dd class="col-sm-4 text-end">2021-11-17</dd>
							</dl>
						</div>
					</div>
				</div>
			</a>
`
	}
}

function nextPage() {
	// if not on last page, goto next page
	if (currentPage < pages) changePage(++currentPage)
}

function prevPage() {
	// if not on the first page, goto previous page
	if (currentPage > 1) changePage(--currentPage)
}

// directly access a page by number
function gotoPage(page) {
	// sets the current page to the selected page
	currentPage = page
	// changes the page to the selected page
	changePage(page)
}

// creates individual page navigation
function addPages() {
	// grab reference to containing element
	var el = document.getElementById('pages')

	el.innerHTML += `<li class="page-item">
			<a class="page-link" href="javascript:prevPage()" aria-label="Föregående" id="btn_prev">
				<span aria-hidden="true">&laquo; Föregående</span>
			</a>
		</li>`
	// for each page
	for (var i = 1; i < pages + 1; i++) {
		// append a link with the respective page number
		el.innerHTML += `<li class="page-item"><a class="page-link" href="javascript:gotoPage(${i})">${i}</a></li>`
	}

	el.innerHTML += `<li class="page-item">
			<a class="page-link" href="javascript:nextPage()" aria-label="Nästa" id="btn_next">
				<span aria-hidden="true">Nästa &raquo;</span>
			</a>
		</li>`
}

// window.onload = function() {
setTimeout(function() {
	changePage(1) // set default page
	addPages() // generate page navigation
}, 200);

// Toggle "active" on pagination

function changeActiveClass() {
	const buttons = document.querySelectorAll('.pagination li');

	for (let i = 0; i < buttons.length; i++) {
		buttons[i].addEventListener("click", () => {

			// Add these two lines:
			const active = document.querySelector('.pagination li.active');
			if (active)
				active.className = '';

			// Updated to use className
			buttons[i].firstElementChild.parentElement.className = 'page-item active';
		});
	}
}
