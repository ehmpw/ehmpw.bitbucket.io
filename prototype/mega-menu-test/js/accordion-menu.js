const initAccordion = (accordionElem) => {
	const handlePanelClick = (event) => {
		showPanel(event.currentTarget);
	};

	const showPanel = (panelHeader) => {
		const panelBody = panelHeader.nextElementSibling;
		const expandedPanel = document.querySelector(".panel.active");

		const isActive =
			expandedPanel &&
			panelHeader.parentNode.classList.contains("active");

		if (expandedPanel) {
			expandedPanel.querySelector(".acc-body").style.height = null;
			expandedPanel.classList.remove("active");
			expandedPanel.setAttribute("aria-expanded", false);
		}

		if (panelHeader.parentNode && !isActive) {
			panelBody.style.height = panelBody.scrollHeight + "px";
			panelHeader.parentNode.classList.add("active");
			panelHeader.parentNode.setAttribute("aria-expanded", true);
		}
	};

	const allPanelElements = accordionElem.querySelectorAll(".panel");

	for (const panel of allPanelElements) {
		panel
			.querySelector(".acc-header")
			.addEventListener("click", handlePanelClick);
	}

	showPanel(allPanelElements);
};

const accordion = document.querySelector(".accordion");
initAccordion(accordion);

// Open menu items
const menuItems = document.querySelectorAll("#menu-0");

for (const menuItem of menuItems) {
	menuItem.classList.add("active");
}
