// Wrap the code in a function or module
(function () {
	// Wait for the DOM to fully load before accessing elements
	document.addEventListener("DOMContentLoaded", () => {
		// Retrieve the menu element and store it in a variable
		const megaMenu = document.getElementById("mega-menu");

		// Retrieve the menu toggle element and store it in a variable
		const menuToggle = document.getElementById("menu-toggle");

		// Attach an event listener to the menu toggle
		menuToggle.addEventListener("change", () => {
			// When the value of the menu toggle changes

			// Find the #myToastContainer element
			const myToastContainer = document.querySelector("#myToastContainer");

			// Check if the element exists
			if (myToastContainer) {
				// Remove the element from its parent
				myToastContainer.parentNode.removeChild(myToastContainer);
			}

			// Toggle the "open" class on the menu element based on the toggle's state
			megaMenu.classList.toggle("open", menuToggle.checked);

			const menuCloseAnywhere = document.querySelector("#mega-menu.open");
			if (menuCloseAnywhere) {
				menuCloseAnywhere.addEventListener("click", (event) => {
					// Check if the clicked element or any of its parents have the class "accordion-button"
					if (event.target.closest(".accordion-button")) {
						return; // Exit the event listener without closing the menu
					}
					megaMenu.classList.remove("open");
					menuToggle.checked = false;
				});
			}
		});

		// Close with Escape
		document.addEventListener('keydown', function (event) {
			if (event.key === 'Escape') {
				megaMenu.classList.remove("open");
				menuToggle.checked = false;
			}
		});

	});
})();

/*

# Accordion

The purpose of this code is to close any open accordions when a new accordion is opened, ensuring that only one accordion can be open at a time. It is an alternative to the same functionality built in to Bootstrap. The built in function is easier to use but with slower and animated response.

Source: https://getbootstrap.com/docs/5.3/components/accordion/#always-open

*/

// const accordions = document.querySelectorAll(".accordion-item");
// accordions.forEach(function (accordion) {
// 	accordion.addEventListener("show.bs.collapse", function () {
// 		accordions.forEach(function (otherAccordion) {
// 			if (otherAccordion !== accordion) {
// 				otherAccordion
// 					.querySelector(".accordion-button")
// 					.classList.remove("collapsed");
// 				otherAccordion
// 					.querySelector(".accordion-collapse")
// 					.classList.remove("show");
// 			}
// 		});
// 	});
// });
