const menuToggle = document.querySelector(".menu-toggle");
const menu = document.querySelector(".menu");

menuToggle.addEventListener("change", function () {
	if (this.checked) {
		menu.classList.add("open");
	} else {
		menu.classList.remove("open");
	}
});
