// Wrap the code in a function or module
(function () {
	// Wait for the DOM to fully load before accessing elements
	document.addEventListener("DOMContentLoaded", () => {
		// Retrieve the menu element and store it in a variable
		const megaMenu = document.getElementById("mega-menu");

		// Retrieve the menu toggle element and store it in a variable
		const menuToggle = document.getElementById("menu-toggle");

		// Attach an event listener to the menu toggle
		menuToggle.addEventListener("change", () => {
			// When the value of the menu toggle changes

			// Find the #myToastContainer element
			const myToastContainer = document.querySelector("#myToastContainer");

			// Check if the element exists
			if (myToastContainer) {
				// Remove the element from its parent
				myToastContainer.parentNode.removeChild(myToastContainer);
			}

			// Toggle the "open" class on the menu element based on the toggle's state
			megaMenu.classList.toggle("open", menuToggle.checked);

			const menuCloseAnywhere = document.querySelector("#mega-menu.open");
			if (menuCloseAnywhere) {
				menuCloseAnywhere.addEventListener("click", (event) => {
					// Check if the clicked element or any of its parents have the class "accordion-button"
					if (event.target.closest(".accordion-button")) {
						return; // Exit the event listener without closing the menu
					}
					megaMenu.classList.remove("open");
					menuToggle.checked = false;
				});
			}
		});

		// Detect the number of accordion items and adjust the column classes accordingly. This code will check if there are more than 6 sections. If there are, it adds the classes row, container-fluid, and mx-auto to the ul element with the id mega-menu-accordion. It then creates three ul elements with the class col-md-4 as columns. It distributes the accordion items evenly among the three columns. If there are 6 or fewer sections, it removes the row class from the ul element.

		const sections = Array.from(
			document.querySelectorAll(".accordion-item")
		);

		if (sections.length > 6) {
			const accordion = document.getElementById("mega-menu-accordion");
			accordion.classList.add("row");
			accordion.innerHTML = `<li class="row container-fluid mx-auto">
				<ul class="col-md-4">
				</ul>
				<ul class="col-md-4">
				</ul>
				<ul class="col-md-4">
				</ul>
			</li>`;
			const columnLists = accordion.querySelectorAll("ul.col-md-4");

			sections.forEach((section, index) => {
				const columnIndex = index % 3;
				columnLists[columnIndex].appendChild(section);

				const headingId = `heading${index + 1}`;
				const collapseId = `collapse${index + 1}`;

				const heading = section.querySelector(".accordion-header");
				const collapse = section.querySelector(".accordion-collapse");

				heading.id = headingId;
				collapse.id = collapseId;
				heading.setAttribute("data-bs-target", `#`);
				collapse.setAttribute("aria-labelledby", headingId);
			});
		} else {
			document
				.getElementById("mega-menu-accordion")
				.classList.remove("row");
		}

		// Close with Escape
		document.addEventListener('keydown', function (event) {
			if (event.key === 'Escape') {
				megaMenu.classList.remove("open");
				menuToggle.checked = false;

			}
		});

	});
})();

/*

# Accordion

The purpose of this code is to close any open accordions when a new accordion is opened, ensuring that only one accordion can be open at a time. It is an alternative to the same functionality built in to Bootstrap. The built in function is easier to use but with slower and animated response.

Source: https://getbootstrap.com/docs/5.3/components/accordion/#always-open

*/

// const accordions = document.querySelectorAll(".accordion-item");
// accordions.forEach(function (accordion) {
// 	accordion.addEventListener("show.bs.collapse", function () {
// 		accordions.forEach(function (otherAccordion) {
// 			if (otherAccordion !== accordion) {
// 				otherAccordion
// 					.querySelector(".accordion-button")
// 					.classList.remove("collapsed");
// 				otherAccordion
// 					.querySelector(".accordion-collapse")
// 					.classList.remove("show");
// 			}
// 		});
// 	});
// });
