// quick search regex
var qsRegex;
var filterSelector = [];

// init Isotope
var isoGrid = document.querySelector(".grid");
if (isoGrid) {
	var iso = new Isotope(isoGrid, {
		itemSelector: ".element-item",
		layoutMode: "masonry",
		columnWidth: 1140,
		filter: function(itemElem) {
			// console.log("itemElem: " + itemElem);

			var filterRes = true;
			if (filterSelector.length > 0) {
				filterRes = arrayHasMatch(filterSelector, itemElem.dataset.cat.split(' '));
			}
			return filterRes;

		}
	});
}

function arrayHasMatch(a1, a2) {
	// console.log(typeof a1);
	var result = a1.filter(el => a2.includes(el));
	if (result.length > 0) {
		return true
	}
	return false;
}

// bind filter button click
var filtersElem = document.querySelector(".filters-button-group");
if (filtersElem) {
	filtersElem.addEventListener("click", function(event) {
		// only work with buttons
		if (!matchesSelector(event.target, "button")) {
			return;
		}
		event.target.classList.toggle('active');
		var isChecked = event.target.classList.contains('active');
		var filter = event.target.getAttribute('data-filter');
		if (isChecked) {
			addFilter(filter);

			var checked = document.createElement("span");
			checked.setAttribute("class", "material-icons ps-2 float-end checkbox-icon");
			checked.innerHTML = "done";

			event.target.appendChild(checked);

		} else {
			removeFilter(filter);

			// console.log(event.target.classList);
			// var children = event.target.childNodes;


			if (event.target.hasChildNodes()) {
				event.target.removeChild(event.target.children[0]);
			}

		}
		// console.log(filterSelector);
		iso.arrange();
	});
}

function addFilter(filter) {
	if (filterSelector.indexOf(filter) == -1) {
		filterSelector.push(filter);
	}
}

function removeFilter(filter) {
	var index = filterSelector.indexOf(filter);
	if (index != -1) {
		filterSelector.splice(index, 1);
	}
}

function debounce(fn, threshold) {
	var timeout;
	threshold = threshold || 100;
	return function debounced() {
		clearTimeout(timeout);
		var args = arguments;
		var _this = this;

		function delayed() {
			fn.apply(_this, args);
		}
		timeout = setTimeout(delayed, threshold);
	};
}
