// Bootstrap tooltips - START
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
	return new bootstrap.Tooltip(tooltipTriggerEl)
})
// Bootstrap tooltips - STOP

// Remove element on event (like click)
function remove(el) {
	var element = el;
	element.remove();
}

// Make page dim nicely when search is in focus

function mainSearchFocus() {
	var main = document.getElementById("main");
	main.style.zIndex = "-2";
}

function mainSearchBlur() {
	var main = document.getElementById("main");
	main.style.zIndex = "0";
}
// var hamburgerToggler = document.querySelector(".hamburger");
// // Detect all clicks on the document
// document.addEventListener("click", function(event) {
// 	// If user clicks inside the element, do nothing
// 	if (event.target.closest(".hamburger-toggler")) return;
// 	// If user clicks outside the element, hide it!
// 	hamburgerToggler.classList.remove("active");
// });
